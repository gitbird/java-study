/**   
 * @Title: FileList.java 
 * @Package com.wowotuan.unit_003 
 * @author liukui<liukui@55tuan.com>
 * @date 2015年6月9日 下午10:41:22 
 * @version V1.0   
 */
package com.wowotuan.unit_003;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.Normalizer.Form;

/**
 * @ClassName: FileList
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author liukui<liukui@55tuan.com>
 * @date 2015年6月9日 下午10:41:22
 * 
 */
public class FileList {

	/**
	 * @param from
	 *            要拷贝的文件或目录
	 * @throws IOException
	 * */
	public void findFileList(String from,String to) throws IOException {
		File fileFrom = new File(from);
		File fileTo = new File(to);
		
		if (!fileFrom.exists()) {
			throw new IOException("file is not exist.");
		}
		if(!fileTo.exists()){
			fileTo.mkdirs();
		}
		
		if (fileFrom.isDirectory()) {
			File[] files = fileFrom.listFiles();
			for (File f : files) {
				if (f.isDirectory()) {
					this.findFileList(f.getPath(),to);
				} else {
					File sfile = new File(f.getPath());
					File tfile = new File(to+File.separator+sfile.getName());
					 this.copy(sfile, tfile);
				}
			}
		} else {
			File sfile = new File(from);
			File tfile = new File(to+File.separator+sfile.getName());
			 this.copy(sfile, tfile);
		}
	}

	public void copy(File from, File to) {
		InputStream fis = null;
		OutputStream fos = null;
		try {
			fis = new BufferedInputStream(new FileInputStream(from));
			fos = new BufferedOutputStream(new FileOutputStream(to));
			// 这只一个字节码缓冲区，用于提高读取和写入文件的速率
			byte[] buff = new byte[2048];
			int i = 0;
			while ((i = fis.read(buff)) != -1) {
				// 注意看write的api【alt+\】的参数说明
				fos.write(buff, 0, i);
			}
		}
		/**
		 * 此种异常扑捉写法是为大家展示try ... catch 代码块应该会有这些异常出现，如果你需要处理某种特定异常
		 * 的时候可以按照这种写法，如：FileNotFoundException 明确标明是文件不存在；大家想想这种写法的好处
		 * 是什么？有什么不好的地方？
		 * */
		catch (FileNotFoundException e) {
			System.err.println("文件不存在或路径不正确:" + e.getMessage());
		} catch (IllegalArgumentException e) {
			System.err.println("数组越界了:" + e.getMessage());
		} catch (IOException e) {
			System.err.println("IO错误:" + e.getMessage());
		} catch (SecurityException e) {
			System.err.println("安全性错误:" + e.getMessage());
		}
		/**
		 * 此种写法是比较是较为常用的，因为大多数情况下我们只需要知道出错了，人工捕捉处理就行了，具体错误看日志就行了
		 * */
		// catch(Exception e){
		// e.printStackTrace();
		// }
		finally {// 不管上面怎么报错，这里都会执行
			try {
				fis.close();
				fos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		FileList filelist = new FileList();
		try {
			filelist.findFileList("/tmp/temp","/tmp/aaa");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("程序执行完毕");
	}
}
