/**   
* @Title: InputCode.java 
* @Package com.wowotuan.unit_002 
* @author liukui<liukui@55tuan.com>
* @date 2015年6月7日 上午11:36:43 
* @version V1.0   
*/
package com.wowotuan.unit_002;

import java.util.Scanner;

/** 
 * @ClassName: InputCode 
 * @Description: 练习字节流的输入输出
 * @author liukui<liukui@55tuan.com> 
 * @date 2015年6月7日 上午11:36:43 
 *  
 */
public class InputCode {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);//创建输入流扫描器
		System.out.println("请输入您的身份证号码：");//提示用户输入
		String line = scanner.nextLine();//等待用户输入信息（按照行扫描），回车后继续执行
		//打印用户输入的信息
		System.out.println("GOGA,原来您的身份证号码是"+line.length()+"位啊！");
		//看下System.out和System.err有什么不同
		System.err.println("GOGA,原来您的身份证号码是"+line.length()+"位啊！");
	}
}
