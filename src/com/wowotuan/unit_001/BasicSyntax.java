/**   
* @Title: BasicSyntax.java 
* @Package com.wowotuan.secret 
* @author liukui<liukui@55tuan.com>
* @date 2015年6月6日 上午11:57:40 
* @version V1.0   
*/
package com.wowotuan.unit_001;

/** 
 * @ClassName: BasicSyntax 
 * @Description: 
 * 1、本示例演示了（标识符、修饰符、变量、枚举、数组、关键字、注解）
 * 2、了解同一个源文件中多个类的写法和使用方法
 * 3、数组的定义和初始化
 * 4、对java8中基本类型的使用
 * 
 * @author liukui<liukui@55tuan.com> 
 * @date 2015年6月6日 上午11:57:40 
 *  
 */
public class BasicSyntax {
	
	
	/**
	 * 数组定义
	 * */
	public void arrayDef(){
		int[] a = {1,2,3}; //定义并初始化值
		int[] b = new int[3];//定义并初始化内存分配
		int[] c; //定义一个数组类型的引用
		int d[]; //定义一个数组类型的引用
		int[] e = null; //定义一个数组类型的引用它指向null
	}
	/**
	 * 8中基本类型的定义
	 * */
	public void baseTypeDef(){
		boolean flag = true;  
        char yeschar = 'y';  
        byte finbyte = 30;  
        int intvalue = -70000;  
        long longvalue = 200;  
        short shortvalue = 20000;  
        float floatvalue = 9.997f;  
        double doublevalue = 1.117;  
          
        System.out.println("The values are:");  
        System.out.println("布尔类型变量    flag:"+flag);  
        System.out.println("字符类型变量    yeschar:"+yeschar);  
        System.out.println("字节类型变量     finbyte:"+finbyte);  
        System.out.println("整型变量    intvalue:"+intvalue);  
        System.out.println("长整型变量    longvalue:"+longvalue);  
        System.out.println("短整型变量    shortvalue:"+shortvalue);  
        System.out.println("浮点类型变量    floatvalue:"+floatvalue);  
        System.out.println("双精度浮点型变量    doublevalue:"+doublevalue); 
	}
	
	
	/** 
	 * <p>Title: </p> 
	 * <p>Description: </p> 
	 * @param args 
	 */
	public static void main(String[] args) {
		//这一行注释TODO的意思是有未完成的待处理的程序需要完善
		// TODO Auto-generated method stub
		
		
	}

}
 
/**
 * 定义了一个类：关键字缺省可以理解为friendly，friendly不是java的关键字
 * 1、缺省包访问权限
 * 2、同一个包内其它类可以访问，但包外就不可以
 * 3、对于同一个文件夹下的、没有用package的classes，Java会自动将这些classes初见为隶属于该目录的default package，可以相互调用class中的friendly成员
 * */
class B{
	
}
