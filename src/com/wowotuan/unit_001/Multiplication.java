/**   
* @Title: Multiplication.java 
* @Package com.wowotuan.secret 
* @author liukui<liukui@55tuan.com>
* @date 2015年6月7日 上午10:05:58 
* @version V1.0   
*/
package com.wowotuan.unit_001;

/** 
 * @ClassName: Multiplication 
 * @Description: 乘法口诀表输出
 * 
 * 	1、本节主要练习控制语法和输出
 * 	2、学习System.out类输出函数的用法
 * 
 * @author liukui<liukui@55tuan.com> 
 * @date 2015年6月7日 上午10:05:58 
 *  
 */
public class Multiplication {

	
	public static void main(String[] args) {
		//乘法口诀表
		for (int i = 1; i <= 9; i++) {
			for (int j = 1; j <=i; j++) {
				System.out.printf(i+"*"+j+"=%2d%2s",i*j,"");//程序格式化输出，使用C语言中的输出格式
			}
			System.out.println();
		}
	}
}
