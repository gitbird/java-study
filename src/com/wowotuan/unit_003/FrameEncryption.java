/**   
 * @Title: FrameEncryption.java 
 * @Package com.wowotuan.unit_003 
 * @author liukui<liukui@55tuan.com>
 * @date 2015年6月11日 下午9:54:31 
 * @version V1.0   
 */
package com.wowotuan.unit_003;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * @ClassName: FrameEncryption
 * @Description: 
 * 	1、练习图形界面开发，熟悉程序开发流程
 * 	2、为图形界面增加事件操作
 * 	3、熟悉事件添加注册流程
 * 	4、优化程序和界面，使其更加美观
 *  作业：
 *  	1、完善图形界面布局，了解熟悉界面布局方式
 *  	2、完善图形界面的各个元素事件操作，实现在图形界面中进行【文件选择】、【密码生成】、【加密解密】过程并将日志输出到
 *  	  界面控制台
 *  	3、扩展练习：增加更丰富的功能，如：文件查找、目录扫描等操作并输出到图形控制台
 * @author liukui<liukui@55tuan.com>
 * @date 2015年6月11日 下午9:54:31
 * 
 */
public class FrameEncryption extends JFrame {

	JFrame jFrame;

	JTextField fromText;
	JTextField toText;

	JButton fromBtn;
	JButton toBtn;
	JButton createPwd;
	JButton ok;

	JTextArea textArea;
	JComboBox comboBox;

	public FrameEncryption() {
		jFrame = new JFrame("牛逼的加密工具");

		jFrame.setLayout(new BorderLayout());
		// 南北布局
		JPanel panelNorth = new JPanel();
		panelNorth.setBackground(Color.GRAY);

		JPanel panelCenter = new JPanel();

		/**** 创建输入源 ********/
		// 源文件
		fromText = new JTextField("", 10);
		fromText.setEditable(false);
		// 新增事件监听
		fromBtn = new JButton("From");
		fromBtn.setFont(new Font("新宋体", Font.BOLD, 12));

		// 目的地
		toText = new JTextField("", 10);
		toText.setEditable(false);
		toBtn = new JButton("To");
		toBtn.setFont(new Font("新宋体", Font.BOLD, 12));

		// 选择密码

		comboBox = new JComboBox();
		comboBox.addItem("请选择密码文件");

		// 确定操作按钮
		createPwd = new JButton("生成密码");
		ok = new JButton("加密解密");

		// 依次将新建的操作元素加入面板（幕布）中
		panelNorth.add(fromText);
		panelNorth.add(fromBtn);
		panelNorth.add(toText);
		panelNorth.add(toBtn);
		panelNorth.add(comboBox);
		panelNorth.add(createPwd);
		panelNorth.add(ok);

		jFrame.add(panelNorth, BorderLayout.NORTH);

		/**** 生成一个TextArea文本区域 ************/
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBackground(Color.LIGHT_GRAY);
		// 当TextArea里的内容过长时生成滚动条
		panelCenter.add(new JScrollPane(textArea));
		panelCenter.setLayout(new GridLayout());

		jFrame.add(panelCenter, BorderLayout.CENTER);

		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.setSize(800, 400);

		// 窗体居中
		Dimension displaySize = Toolkit.getDefaultToolkit().getScreenSize();
		double x = (displaySize.getWidth() - jFrame.getWidth()) / 2;
		double y = (displaySize.getHeight() - jFrame.getHeight()) / 2;

		jFrame.setLocation((int) x, (int) y);
		jFrame.setVisible(true);

		// 註冊事件
		this.addFromTextEvent();
		this.addCreatePwdEvent();
	}

	/**
	 * 生成密码文件
	 * */
	private String createPwd() {
		String fileName = "";
		Encryption encrypt = new Encryption();
		try {
			String pwdDir = "pwd";
			File dir = new File(pwdDir);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			int fileCount = dir.list().length;
			fileName = (++fileCount) + ".pwd";
			String pwdFilePath = pwdDir + File.separator + fileName;
			this.textArea.append("生成密碼：" + pwdFilePath + "\n");
			// System.out.println(pwdFilePath);
			encrypt.createKeyFile(pwdFilePath);

		} catch (Exception e) {
		}
		return fileName;
	}

	private void addCreatePwdEvent() {
		// 装载密码
		File file = new File("pwd");
		if(!file.exists()){
			file.mkdirs();
		}
		for (String pwdPath : file.list()) {
			System.out.println(pwdPath);
			this.comboBox.addItem(pwdPath);
		}
		MouseListener ml = new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				String fileName = createPwd();
				comboBox.addItem(fileName);
			}
		};
		this.createPwd.addMouseListener(ml);
	}

	/**
	 * 选择源文件添加事件
	 * */
	private void addFromTextEvent() {
		MouseListener ml = new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// textArea.append("random number:"+Math.random()+"\n");
				JFileChooser choser = new JFileChooser();
				if (choser.showOpenDialog(FrameEncryption.this) == JFileChooser.APPROVE_OPTION) {
					String filePath = choser.getSelectedFile()
							.getAbsolutePath();
					fromText.setText(filePath);
					textArea.append("你选择了文件：" + filePath + "\n");
				}
			}
		};
		this.fromBtn.addMouseListener(ml);
	}

	private void setFrameLayout(JFrame jFrame) {
		jFrame.setLayout(new BorderLayout());
		jFrame.add(new Button("North"), BorderLayout.NORTH);
		jFrame.add(new Button("South"), BorderLayout.SOUTH);
		jFrame.add(new Button("East"), BorderLayout.EAST);
		jFrame.add(new Button("West"), BorderLayout.WEST);
		jFrame.add(new Button("Center"), BorderLayout.CENTER);
	}

	/**
	 * <p>
	 * Title:
	 * </p>
	 * <p>
	 * Description:
	 * </p>
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new FrameEncryption();
	}

}
