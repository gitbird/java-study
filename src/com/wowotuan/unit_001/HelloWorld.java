/**   
* @Title: Test.java 
* @Package com.wowotuan.unit_001 
* @author liukui<liukui@55tuan.com>
* @date 2015年6月7日 上午10:53:30 
* @version V1.0   
*/
package com.wowotuan.unit_001;

/** 
 * @ClassName: Test 
 * @Description: 第一个程序示例，注意：
 * 	1、类的写法
 * 	2、类入口函数的写法，注意main(String[] args) 该函数的参数是一个数组，是可以用来接受外界命令行等参数信息的，自己可以试一试
 * @author liukui<liukui@55tuan.com> 
 * @date 2015年6月7日 上午10:53:30 
 *  
 */
public class HelloWorld {

	public static void main(String[] args) {
		System.out.println("Hello World!");
	}
}
