/**   
* @Title: RedirectOutputStream.java 
* @Package com.wowotuan.unit_002 
* @author liukui<liukui@55tuan.com>
* @date 2015年6月7日 上午11:45:43 
* @version V1.0   
*/
package com.wowotuan.unit_002;

import java.io.PrintStream;

/** 
 * @ClassName: RedirectOutputStream 
 * @Description: 本例练习将用户输入重定向输出到文件
 * 1、练习使用异常捕获
 * 2、文件操作，文件操作有好几种方式，这里只写一种，其余的可以作为练习题目进行自己百度搜索
 * 3、注意：所有IO操作和其他未知的第三方调用都会有异常抛出，必须进行异常处理
 * 4、在使用每个类的函数时，请看清楚他的api用法和是否有异常等说明
 * 
 * @author liukui<liukui@55tuan.com> 
 * @date 2015年6月7日 上午11:45:43 
 *  
 */
public class RedirectOutputStream {

	/** 
	 * <p>Title: </p> 
	 * <p>Description: 
	 * 	扩展练习：
	 * 		思考查找其他的方式进行文件字符流操作，百度、google
	 * </p> 
	 * @param args 
	 */
	public static void main(String[] args) {
		try {
			PrintStream out = System.out; //保存原输出流
			PrintStream ps = new PrintStream("./log.txt");//创建文件输出流，注意查看该函数的其他多态特性使用
			System.setOut(ps);
			
			int age = 28;//定义一个变量
			System.out.println("年龄变量定义，初始值 18");
			String sex = "男";
			System.out.println("年龄变量定义，初始值 女");
			//整合2个变量
			String info = sex+" 孩子你都已经 "+age+" 岁了，学程序是不是有点晚了";
			System.out.println("两个变量在一起输出："+info);
			System.setOut(out);
			System.out.println("程序运行完毕，请查看日志文件");
			
		} catch (Exception e) {
			//异常处理在这里
		}
	}

}
