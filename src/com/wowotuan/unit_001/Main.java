/**   
* @Title: Test01.java 
* @Package com.wowotuan.unit_002 
* @author liukui<liukui@55tuan.com>
* @date 2015年6月7日 上午10:57:19 
* @version V1.0   
*/
package com.wowotuan.unit_001;

/** 
 * @ClassName: Test01 
 * @Description: 程序入口函数的研究
 * @author liukui<liukui@55tuan.com> 
 * @date 2015年6月7日 上午10:57:19 
 *  
 */
public class Main {

	/** 
	 * <p>Title: </p> 
	 * <p>Description: 本节我们着重练习程序入口函数的使用
	 *	1、命令行模式下输入多个参数查看输出结果
	 *	2、javac com/wowotuan/unit_002/Main01.java
	 *	3、java com/wowotuan/unit_002/Main01 hello world
	 * </p> 
	 * @param args 
	 */
	public static void main(String[] args) {
		for (int i = 0; i < args.length; i++) {
			System.out.println("第["+i+"]个参数："+args[i]);
		}
	}

}
