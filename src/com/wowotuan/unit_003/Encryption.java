/**   
* @Title: Encryption.java 
* @Package com.wowotuan.unit_003 
* @author liukui<liukui@55tuan.com>
* @date 2015年6月10日 下午10:49:14 
* @version V1.0   
*/
package com.wowotuan.unit_003;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/** 
 * @ClassName: Encryption
 *  
 * @Description: 该类是文件二进制按位加密解密
 * 1、目前加密解密程序没有使用缓冲区，效率比较低，请改成缓冲区方式以提高效率
 * 2、想一想，如果你拿不到生成的这个密码文件，该文件是否有机会进行解密
 *  
 * @author liukui<liukui@55tuan.com> 
 * @date 2015年6月10日 下午10:49:14 
 *  
 */
public class Encryption {

	private byte[] createRandomKey(int keyLength){
		byte[] key = new byte[keyLength];
		for (int i = 0; i < key.length; i++) {
			key[i] = (byte) (i&(int)(Math.random()*key.length));
		}
		return key;
	}
	/**
	 * 生成一个密码文件，完成一次加密解密过程都将使用同一个文件，否则你所操作的是多次进行加密
	 * */
	public void createKeyFile(String filePath) throws Exception{
		this.createKeyFile(filePath, 256);
	}
	/**
	 * 生成一个密码文件，完成一次加密解密过程都将使用同一个文件，否则你所操作的是多次进行加密
	 * */
	public void createKeyFile(String filePath,int keyLength) throws Exception{
		File file = new File(filePath);
		FileOutputStream fos = new FileOutputStream(file);
		byte[] key = this.createRandomKey(keyLength);
		fos.write(key);
		fos.close();
	}
	/**
	 * 文件加密
	 * */
	public void fileEncryption(String fromPath,String toPath,String keyPath) throws Exception{
		File from = new File(fromPath);
		File to = new File(toPath);
		//读取密码
		FileInputStream key = new FileInputStream(new File(keyPath));
		int keyLength = key.available();
		byte[] buff = new byte[keyLength];
		key.read(buff);
		
		FileInputStream fis = new FileInputStream(from);
		FileOutputStream fos = new FileOutputStream(to);
		int fileSize = fis.available();
		System.out.println("文件大小："+fileSize+",密码文件大小："+buff.length);
		int tempbyte;
		int i=0;
		while((tempbyte = fis.read())!=-1){
			fos.write(tempbyte+buff[i%buff.length]);
//			fos.write(tempbyte+1);
			i++;
		}
		fis.close();
	}
	/**
	 * 文件解密
	 * */
	public void fileDeEncryption(String fromPath,String toPath,String keyPath) throws Exception{
		File from = new File(fromPath);
		File to = new File(toPath);
		//读取密码
		FileInputStream key = new FileInputStream(new File(keyPath));
		int keyLength = key.available();
		byte[] buff = new byte[keyLength];
		key.read(buff);
		
		FileInputStream fis = new FileInputStream(from);
		FileOutputStream fos = new FileOutputStream(to);
		int fileSize = fis.available();
		System.out.println("文件大小："+fileSize+",密码文件大小："+buff.length);
		int tempbyte;
		int i=0;
		while((tempbyte = fis.read())!=-1){
			fos.write(tempbyte-buff[i%buff.length]);
			i++;
		}
		fis.close();
	}
	/** 
	 * <p>Title: </p> 
	 * <p>Description: </p> 
	 * @param args 
	 */
	public static void main(String[] args) {
		Encryption encryp = new Encryption();
		String keyPath = "/tmp/random.key";
		try {
			encryp.createKeyFile(keyPath,256);
			encryp.fileEncryption("/Users/liukui/Downloads/memcached-1.4.24.tar.gz", "/tmp/memcached-1.4.24.tar.gz",keyPath);
			encryp.fileDeEncryption("/tmp/memcached-1.4.24.tar.gz", "/tmp/de-memcached-1.4.24.tar.gz",keyPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
